:: Qt Installer Framework bin directory should be in the Path variable
binarycreator.exe --offline-only -c config\config.xml -p packages MyTimeOfflineInstaller.exe
binarycreator.exe --online-only -c config\config.xml -p packages MyTimeOnlineInstaller.exe